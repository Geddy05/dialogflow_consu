# -*- coding: utf-8 -*-

import json
from flask import make_response


class Response:
    def __init__(self,request):
        jn = json.loads(request)
        result = jn['result']
        self.action = result['action']
        self.fulfillment = result['fulfillment']['speech']
        self.parameters = result['parameters']
        self.resolvedQuery = result['resolvedQuery']
        self.inputContexts = result['contexts']

    def createResponse(self,response):
        res = json.dumps(response)
        r = make_response(res)
        r.headers['Content-Type'] = 'application/json'

        return r