# -*- coding: utf-8 -*-

import logging as l

from flask import Flask
from flask import request
from intents import compareInsurance , sign_up

from handlers.dialogflowResponse import Response

app = Flask(__name__)

# Note: We don't need to call run() since our application is embedded within
# the App Engine WSGI application server.

def changeName(response):
    """ trigger follow up event wegenwacht """

    print(response.parameters)
    my_response ={
        'followupEvent' : {
            'name' : 'lidworden',
            'data': response.parameters
        }
    }

    return my_response


# Action mapping. navigate to the right functions for a action.
routing = {
    'input.vergelijken': compareInsurance.CompareInsurance,
    'input.lidworden': sign_up.sign_up,
    'input.select_contract' : sign_up.select_contract,
    'input.change_name': changeName
}


@app.route('/')
def hello():
    """Return a friendly HTTP greeting."""
    l.info("reached the hello() module...")

    return 'Hello Consumentenbond!\n'


@app.route('/', methods=['POST'])
def apiai_response():

    """ Create a request object"""
    response = Response(request.data)

    """ Call the function from the routing dict and pass the parameters"""
    my_response = routing[response.action](response)

    """ Create the response and send it back"""
    return response.createResponse(my_response)


@app.errorhandler(404)
def page_not_found(e):
    """Return a custom 404 error."""
    return 'Sorry, nothing at this URL.', 404


