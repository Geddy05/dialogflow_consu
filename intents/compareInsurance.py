# -*- coding: utf-8 -*-
from entities import EigenRisico as risico
from entities import dekking
from entities import keuzevrijheid as keuze

def CompareInsurance(response):

    if response.parameters["gender"] == '':
        my_response = select_gender()

    elif response.parameters["postcode"] == '':
        my_response = {
            "speech": "En wat is je postcode?",
            "displayText ": "En wat is je postcode?",
        }

    elif response.parameters["geboortedatum"] == '':
        my_response = {
            "speech": "Graag zou ik ook je geboortedatum weten. Wat is je geboortedatum?",
            "displayText ": "Graag zou ik ook je geboortedatum weten. Wat is je geboortedatum?",
        }

    elif response.parameters["keuzevrijheid"] == '':
        my_response = keuzevrijheid()

    elif response.parameters["eigen_risico"] == '':
        my_response = eigen_risico()

    elif response.parameters["aanvullende_dekkingen"] == '':

        speech = "En als laatste, welke aanvullende dekkingen denk je nodig te hebben het komende jaar? " \
                 "Bijv. Fysiotherapie, brillen en lenzen of alternatieve geneeswijzen. Het is mogelijk om meerdere te kiezen"

        my_response = {
            "speech": speech,
            "displayText ": speech,
        }

    else:
        print response
        speech = create_output(response)
        my_response = {
            'messages': [
                speech
            ]
        }

    return my_response


def select_gender():
    speech = "Om je goed te kunnen helpen bij het vergelijken van zorgverzekeringen," \
             " wil ik graag een aantal dingen van je weten. Ben je een man of een vrouw?"

    my_response = {
        'messages': [{
            'speech': speech,
            'answers': [{
                'name': 'Man',
                'msg': 'Man',
            }, {
                'name': 'Vrouw',
                'msg': 'Vrouw',
            }]
        }]
    }
    return my_response


def keuzevrijheid():
    speech = "Zorgverzekeringen verschillen in keuzevrijheid voor zorgverleners." \
             " Vaak geldt hoe ruimer de zorgkeuze, hoe duurder je zorgverzekering." \
             " Wil je volledige keuze vrijheid of heb je geen voorkeur?"

    my_response = {
        'messages': [{
            'speech': speech,
            'answers': [{
                'name': keuze.volledige,
                'msg': keuze.volledige,
            }, {
                'name': keuze.geen,
                'msg': keuze.geen,
            }]
        }]
    }

    return my_response


def eigen_risico():
    speech = "Hoeveel eigen risico wil je volgend jaar?"

    my_response = {
        'messages': [{
            'speech': speech,
            'answers': [{
                'name': risico.e385,
                'msg': risico.e385,
            }, {
                'name': risico.e485,
                'msg': risico.e485,
            }, {
                'name': risico.e585,
                'msg': risico.e585,
            }, {
                'name': risico.e685,
                'msg': risico.e685,
            }, {
                'name': risico.e785,
                'msg': risico.e785,
            }, {
                'name': risico.e885,
                'msg': risico.e885,
            }]
        }]
    }

    return my_response


def create_output(response):
    dekking_input = response.parameters["aanvullende_dekkingen"]
    risico_input = response.parameters["eigen_risico"]
    keuzen_input = response.parameters["keuzevrijheid"]

    print (dekking_input)
    print (risico_input)
    print (keuzen_input)

    if dekking.geen in dekking_input and risico_input == risico.e385 and keuzen_input == keuze.volledige:
        return {
            "type": 3,
            "imageUrl": "https://storage.googleapis.com/indoor_positioning/2017-11-17.png",
            "href": "https://www.consumentenbond.nl/zorgverzekering/vergelijker#eyJhcHBsaWNhbnQiOnsiYSI6eyJnZW5kZXIiOiJtYWxlIiwiZmVtYWxlIjpmYWxzZSwibWFsZSI6dHJ1ZSwiYmlydGhkYXRlIjoiMjAtMTEtMTk4OSIsInZhbGlkQmlydGhkYXRlIjp0cnVlLCJ5ZWFyIjoiMTk4OSIsIm1vbnRoIjoiMTEiLCJkYXkiOiIyMCJ9LCJwIjpudWxsLCJjIjpbXSwidmFsaWQiOnRydWUsIm9wZW4iOmZhbHNlfSwib3ducmlzayI6eyJ2Ijp0cnVlLCJvIjpmYWxzZSwiciI6IjM4NSJ9LCJoZWFsdGhjYXJlIjp7Im8iOmZhbHNlLCJ2Ijp0cnVlLCJhIjoiZnJlZSIsImIiOltdLCJjIjpbXSwiZCI6eyJwb3N0Y29kZSI6IjM1NTJFTiIsInR5cGUiOiJ6aWVrZW5odWlzIiwiaW5zdXJhbmNlSWQiOm51bGx9LCJlIjoiVm9sbGVkaWdlIGtldXpldnJpamhlaWQiLCJ6IjpmYWxzZSwidCI6ZmFsc2V9LCJpbnN1cmFuY2UiOnsibyI6dHJ1ZSwicyI6InByZW1pZSIsInkiOiJmYWxzZSIsInAiOjEsImkiOiJUb29uIGFsbGUgdmVyemVrZXJhYXJzIiwiZiI6eyJudW1iZXJPZlJlc3VsdHMiOjIwLCJsb3dlc3RQcmljZSI6MTA3LjgyLCJmb3JtYXR0ZWRMb3dlc3RQcmljZSI6IuKCrCAxMDcsODIifSwic3QiOmZhbHNlLCJoY2IiOnRydWV9LCJhZGRpdGlvbmFsQ292ZXJhZ2UiOnsibyI6ZmFsc2UsImMiOltdLCJhIjpmYWxzZSwib3AiOnRydWV9LCJjb21wYXJlIjp7InMiOltdfSwiY29tcGFyZVZpZXciOnsidCI6ZmFsc2V9LCJkZXRhaWwiOnsibyI6ZmFsc2V9fQ=="
        }

    else:
        return {"speech" :response.fulfillment}