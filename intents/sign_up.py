def sign_up(response):
    params = response.parameters

    if params['abonnementen'] =='':
        my_response = {
            'messages': [{
                'speech': "Wat leuk dat je lid wilt worden van de consumentenbond. Welk abonnement wil je?",
                'answers': [{
                    'name': 'Extra abonnement',
                    'msg': 'Extra abonnement',
                }, {
                    'name': 'Basis abonnement',
                    'msg': 'Basis abonnement',
                }]
            }]
        }
    else:
        if params['name'] == '':
            speech = "Om de bestellingen compleet te maken heb ik een aantal gegevens nodig. Wat is je volledige naam?"
        elif params['postcode'] == '':
            speech = "En wat is je postcode?"
        elif params['huisnummer'] == '':
            speech = "En welk huisnummer hoort daarbij?"
        elif params['telefoon'] == '':
            speech = "Op welk telefoonnummer kunnen we je bereiken?"
        elif params['bank'] == '':
            speech = "De eerste 2 maanden zijn gratis. Daarna betaal je via automatische incasso. ' \
                               'Je kunt elke maand opzeggen. Als je binnen 2 maanden opzegt, ' \
                               'wordt er niets van je rekening afgeschreven. Wat is je bankrekening nummer?"
        else:
            speech = response.fulfillment

        my_response = {
            "speech": speech,
            "displayText ": speech,
        }

    return my_response

def select_contract(response):
    params = response.parameters

    if params['abonnementen'] =='':
        my_response = {
            'messages': [{
                'speech': "Wat leuk dat je lid wilt worden van de consumentenbond. Welk abonnement wil je?",
                'answers': [{
                    'name': 'Extra abonnement',
                    'msg': 'Extra abonnement',
                }, {
                    'name': 'Basis abonnement',
                    'msg': 'Basis abonnement',
                }]
            }]
        }
    else:
        my_response = {'followupEvent':
            {
                'name': 'lidworden',
            }
        }

    return my_response